# README #

This is a response to the technical test found here:
https://github.com/zfoltin/interviewer

This solution relies heavily on Konstantin Mikheev's Nucleus libaray, found here:
https://github.com/konmik/nucleus

### How do I get set up? ###

Clone the repo then use Android Studio 'import project from Gradle'. Navigate to the repo root folder.