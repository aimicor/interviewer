####1. How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

Roughly 20 hours, taking out breaks. Not sure I would have done much different - except using my own copyrighted Presenter platform instead of Nucleus (which would actually have saved time). Perhaps better graphics:- tabs with a view pager; better error handling; etc. Maybe more unit-tests. Currently the only tests in there are for those items I felt I needed to test-drive.

####2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.

##Java

I expect everyone will mention Functional Programming features from Java 8 here. Android has only recently moved to a version of Java 7 and does not officially support it. There is a 3rd-party support library called RetroLambda instead. Not used it in anger so far but from what I have seen, this would likely to be favoured by people fond of defining anonymous inner classes - so probably the whole of the Google Android team. Having spent a decade developing C++ before moving to Java, it feels like a step backwards to me.

##Android

Android does not tend to bring out useful new features. Mostly their enhancements are about tidying up their atrocious code base. One such example is RecyclerView. Not particularly new, though. It's not really something that can be demo'd in a snippet, but there are examples at app/src/main/java/com/aimicor/interviewer/uicontroller/adapter/

####3. What is your favourite framework / library / package that you love but couldn't use in the task? What do you like about it so much?

I would have to say that currently, my favourite framework is my own copyrighted presenter platform. I did not use it here because I consider it to give me a competitive advantage as a freelancer and I am not about to give that away just for the sake of a technical test. However, I did use the library's primordial source, Nucleus.

What I like about it is that it uses containment instead of inheritance (unlike Nucleus) so can be tagged onto any flavour of Activity or Fragment and can integrate easily into any custom base controller class. It wraps up all of the repetitive, nasty 'restartable' code that can be seen in the Presenter classes' onCreate methods in this repo. It forces you to decouple UI implementations from Fragments and Activitys (unlike Nucleus that forces tight coupling). This makes it a lot easier to test-drive UI components without running into Android platform 'final's in unit tests. Furthermore, the decoupled UI classes are persisted through device rotations along with the Presenter. This means that you don't need to mess about with save and restore instance states. When used correctly, it can facilitate performance enhancements that are not possible with the traditional arrangement, particularly when dealing with list adapters.

####4. What great new thing you learnt about in the past year and what are you looking forward to learn more about over the next year?

(Assuming 'thing' refers to relevant technology) The learning process never ends so this is a difficult question to answer. Probably most recently Retrofit2 and it's integration with rxJava.

I need to look into rxJava2. Not sure that that qualifies as 'looking forward to'. I'm a bit worried that it will rely on Java 8's Functional properties, and will therefore be relatively useless for Android.

####5. How would you track down a performance issue in an application? Have you ever had to do this?

* If it's UI performance then you get info messages about specific methods taking too long on the main thread in logcat.
* If it's memory performance then the excellent memory profiler in Android Studio helps show up leaks.

Yes, I've had to do this when working with legacy code.

####6. How would you improve the APIs that you just used?

The biggest mistake wrt apis that I have come across is that they are generally written and optimised for websites. The minimum data is returned and you need to make follow-up calls to get the level of data required. For a mobile device, the time to download information, especially if it is json text, is a small fraction compared to the time required to establish a connection. If you have to make a lot of connections, the perception of the app resonse time to the user becomes very poor.  Therefore, more data per call is generally better.

In this example, you need to make one call for the balance and another call for the transactions. I would just have one call, say, /account, that would return something like this:

```json
{
	"balance" : "197.25"
	"currency" : "GBP"
	"transactions" : [
		{
			"id": "123",
			"date": "2016-12-11T12:23:34Z",
			"description": "A bag of spanners",
			"amount": "35.25",
			"currency": "GBP"
		},
		{
			"id": "124",
			"date": "2016-12-12T01:58:59Z",
			"description": "Hot chocholate",
			"amount": "12.50",
			"currency": "GBP"
		},
		... etc.
	]
}
```
Furthermore, instead of /spend returning an empty response, I would have it return the updated /account information in the same format as above. This would save two further api calls. If this had been the case I would have likely had designed a cleaner UI. The current UI design is driven by the existing api calls.

####7. Please describe yourself using JSON.

```json
{
	"name" : "Myles"
	"surname" : "Bennett"
	"occupation" : "Android Freelancer"
	"phone" : "07495301005"
	"email" : "myles.bennett@aimicor.com"
	"url" : "http://www.aimicor.com"
}
```

####8. What is the meaning of life?

http://www.imdb.com/title/tt0085959/

