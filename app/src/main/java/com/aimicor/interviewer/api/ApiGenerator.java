package com.aimicor.interviewer.api;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiGenerator {

    private static final String BASE_URL = "https://interviewer-api.herokuapp.com";

    private final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private final Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

    ApiGenerator() {
        this(new ApiInterceptor());
    }

    private ApiGenerator(Interceptor interceptor) {
        httpClient.addInterceptor(interceptor);
    }

    <T> T createFrom(Class<T> apiClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(apiClass);
    }
}
