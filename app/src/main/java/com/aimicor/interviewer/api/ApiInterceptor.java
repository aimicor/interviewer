package com.aimicor.interviewer.api;

import com.aimicor.interviewer.model.TokenInstance.Token;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class ApiInterceptor implements Interceptor {

    private static final String ACCEPT= "Accept";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";

    private final Token mToken;

    ApiInterceptor() {
        this(new Token());
    }

    private ApiInterceptor(Token token) {
        mToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                .header(ACCEPT, APPLICATION_JSON)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(AUTHORIZATION, mToken.get())
                .method(original.method(), original.body());

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
