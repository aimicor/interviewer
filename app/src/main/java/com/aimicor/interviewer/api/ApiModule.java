package com.aimicor.interviewer.api;

public class ApiModule {

    public static TokenApi tokenApiProvider(){
        return new ApiGenerator().createFrom(TokenApi.class);
    }

    public static BalanceApi balanceApiProvider() {
        return new ApiGenerator().createFrom(BalanceApi.class);
    }

    public static TransactionApi transactionApiProvider() {
        return new ApiGenerator().createFrom(TransactionApi.class);
    }

    public static SpendApi spendApiProvider() {
        return new ApiGenerator().createFrom(SpendApi.class);
    }
}
