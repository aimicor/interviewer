package com.aimicor.interviewer.api;

import com.aimicor.interviewer.model.BalancePojo;

import retrofit2.http.GET;
import rx.Observable;

public interface BalanceApi {

    @GET("/balance")
    Observable<BalancePojo> get();
}
