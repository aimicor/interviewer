package com.aimicor.interviewer.api;

import com.aimicor.interviewer.model.TransactionPojo;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface SpendApi {

    @POST("/spend")
    Observable<Void> post(@Body TransactionPojo transaction);
}
