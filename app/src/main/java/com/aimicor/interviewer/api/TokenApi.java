package com.aimicor.interviewer.api;

import com.aimicor.interviewer.model.TokenInstance.TokenPojo;

import retrofit2.http.POST;
import rx.Observable;

public interface TokenApi {

    @POST("/login")
    Observable<TokenPojo> get();
}
