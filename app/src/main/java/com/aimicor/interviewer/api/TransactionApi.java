package com.aimicor.interviewer.api;

import com.aimicor.interviewer.model.TransactionPojo;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface TransactionApi {

    @GET("/transactions")
    Observable<List<TransactionPojo>> get();
}
