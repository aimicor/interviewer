package com.aimicor.interviewer.model;

public enum TokenInstance {

    INSTANCE;

    private String token = "";

    private String getToken() {
        return token;
    }

    private void setToken(String token) {
        this.token = token;
    }

    public static class TokenPojo {
        @SuppressWarnings("unused")
        private String token;
    }

    public static class Token {

        private final static String BEARER = "Bearer ";

        public void set(TokenPojo tokenPojo) {
            INSTANCE.setToken(BEARER + tokenPojo.token);
        }

        public String get() {
            return INSTANCE.getToken();
        }
    }
}
