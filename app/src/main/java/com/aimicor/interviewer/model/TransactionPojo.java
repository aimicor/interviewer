package com.aimicor.interviewer.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Locale.UK;

public class TransactionPojo {

    public static final String POJO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private final String date;
    private final String description;
    private final String amount;

    private TransactionPojo(Builder builder) {
        amount = builder.amount;
        description = builder.description;
        date = new SimpleDateFormat(POJO_DATE_FORMAT, UK).format(new Date());
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getAmount() {
        return amount;
    }

    public static final class Builder {

        private String amount;
        private String description;

        public Builder() {}

        public Builder amount(CharSequence amount) {
            this.amount = amount.toString();
            return this;
        }

        public Builder description(CharSequence description) {
            this.description = description.toString();
            return this;
        }

        public TransactionPojo build() {
            return new TransactionPojo(this);
        }
    }
}
