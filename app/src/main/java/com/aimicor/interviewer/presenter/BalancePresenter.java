package com.aimicor.interviewer.presenter;

import com.aimicor.interviewer.api.BalanceApi;
import com.aimicor.interviewer.model.BalancePojo;
import com.aimicor.interviewer.uicontroller.BalanceFragment;
import com.aimicor.interviewer.uicontroller.ErrorHandler;

import android.os.Bundle;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import static com.aimicor.interviewer.api.ApiModule.balanceApiProvider;
import static com.aimicor.interviewer.uicontroller.UiControllerModule.errorHandlerProvider;

public class BalancePresenter extends RxPresenter<BalanceFragment> {

    private final BalanceApi mBalanceApi;
    private final ErrorHandler mErrorHandler;
    private final int mRequestId = this.hashCode();

    public BalancePresenter() {
        this(balanceApiProvider(), errorHandlerProvider());
    }

    private BalancePresenter(BalanceApi balanceApi, ErrorHandler errorHandler) {
        mBalanceApi = balanceApi;
        mErrorHandler = errorHandler;
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        restartableLatestCache(mRequestId,
                new Func0<Observable<BalancePojo>>() {
                    @Override
                    public Observable<BalancePojo> call() {
                        return mBalanceApi
                                .get()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                },
                new Action2<BalanceFragment, BalancePojo>() {
                    @Override
                    public void call(BalanceFragment balanceFragment, BalancePojo balancePojo) {
                        balanceFragment.onBalanceRequestSuccess(balancePojo.get());
                    }
                },
                new Action2<BalanceFragment, Throwable>() {
                    @Override
                    public void call(BalanceFragment balanceFragment, Throwable throwable) {
                        mErrorHandler.handleError(balanceFragment.getActivity(), throwable);
                    }
                });
    }

    public void requestBalance(){
        start(mRequestId);
    }
}
