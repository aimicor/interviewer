package com.aimicor.interviewer.presenter;

import com.aimicor.interviewer.api.TokenApi;
import com.aimicor.interviewer.model.TokenInstance.Token;
import com.aimicor.interviewer.model.TokenInstance.TokenPojo;
import com.aimicor.interviewer.uicontroller.ErrorHandler;
import com.aimicor.interviewer.uicontroller.MainActivity;

import android.os.Bundle;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import static com.aimicor.interviewer.api.ApiModule.tokenApiProvider;
import static com.aimicor.interviewer.uicontroller.UiControllerModule.errorHandlerProvider;

public class MainPresenter extends RxPresenter<MainActivity> {

    private final TokenApi mTokenApi;
    private final ErrorHandler mErrorHandler;
    private final Token mToken;
    private final int mRequestId = this.hashCode();

    public MainPresenter() {
        this(tokenApiProvider(), errorHandlerProvider(), new Token());
    }

    private MainPresenter(TokenApi tokenApi, ErrorHandler errorHandler, Token token) {
        mTokenApi = tokenApi;
        mErrorHandler = errorHandler;
        mToken = token;
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        restartableFirst(mRequestId,
                new Func0<Observable<TokenPojo>>() {
                    @Override
                    public Observable<TokenPojo> call() {
                        return mTokenApi
                                .get()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                },
                new Action2<MainActivity, TokenPojo>() {
                    @Override
                    public void call(MainActivity activity, TokenPojo response) {
                        mToken.set(response);
                        activity.onTokenRequestSuccess();
                    }
                },
                new Action2<MainActivity, Throwable>() {
                    @Override
                    public void call(MainActivity mainActivity, Throwable throwable) {
                        mErrorHandler.handleError(mainActivity, throwable);
                    }
                });
    }

    public void requestToken(){
        start(mRequestId);
    }
}
