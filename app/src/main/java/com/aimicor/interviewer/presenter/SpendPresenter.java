package com.aimicor.interviewer.presenter;

import com.aimicor.interviewer.api.SpendApi;
import com.aimicor.interviewer.model.TransactionPojo;
import com.aimicor.interviewer.uicontroller.ErrorHandler;
import com.aimicor.interviewer.uicontroller.SpendFragment;

import android.os.Bundle;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import static com.aimicor.interviewer.api.ApiModule.spendApiProvider;
import static com.aimicor.interviewer.uicontroller.UiControllerModule.errorHandlerProvider;

public class SpendPresenter extends RxPresenter<SpendFragment> {

    private final SpendApi mSpendApi;
    private final ErrorHandler mErrorHandler;
    private final int mRequestId = this.hashCode();

    private TransactionPojo mTransaction;

    public SpendPresenter() {
        this(spendApiProvider(), errorHandlerProvider());
    }

    private SpendPresenter(SpendApi spendApi, ErrorHandler errorHandler) {
        mSpendApi = spendApi;
        mErrorHandler = errorHandler;
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        restartableFirst(mRequestId,
                new Func0<Observable<Void>>() {
                    @Override
                    public Observable<Void> call() {
                        return mSpendApi
                                .post(mTransaction)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                },
                new Action2<SpendFragment, Void>() {
                    @Override
                    public void call(SpendFragment spendFragment, Void aVoid) {
                        spendFragment.onTransactionSendSuccess();
                    }
                },
                new Action2<SpendFragment, Throwable>() {
                    @Override
                    public void call(SpendFragment spendFragment, Throwable throwable) {
                        mErrorHandler.handleError(spendFragment.getActivity(), throwable);
                    }
                }
        );
    }

    public void sendTransaction(TransactionPojo transaction) {
        mTransaction = transaction;
        start(mRequestId);
    }
}
