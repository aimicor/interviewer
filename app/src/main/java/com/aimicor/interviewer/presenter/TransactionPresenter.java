package com.aimicor.interviewer.presenter;

import com.aimicor.interviewer.api.TransactionApi;
import com.aimicor.interviewer.model.TransactionPojo;
import com.aimicor.interviewer.uicontroller.ErrorHandler;
import com.aimicor.interviewer.uicontroller.TransactionFragment;

import android.os.Bundle;

import java.util.List;

import nucleus.presenter.RxPresenter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import static com.aimicor.interviewer.api.ApiModule.transactionApiProvider;
import static com.aimicor.interviewer.uicontroller.UiControllerModule.errorHandlerProvider;

public class TransactionPresenter extends RxPresenter<TransactionFragment> {

    private final TransactionApi mTransactionApi;
    private final ErrorHandler mErrorHandler;
    private final int mRequestId = this.hashCode();

    public TransactionPresenter() {
        this(transactionApiProvider(), errorHandlerProvider());
    }

    private TransactionPresenter(TransactionApi transactionApi, ErrorHandler errorHandler) {
        mTransactionApi = transactionApi;
        mErrorHandler = errorHandler;
    }

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        restartableLatestCache(mRequestId,
                new Func0<Observable<List<TransactionPojo>>>() {
                    @Override
                    public Observable<List<TransactionPojo>> call() {
                        return mTransactionApi
                                .get()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                },
                new Action2<TransactionFragment, List<TransactionPojo>>() {
                    @Override
                    public void call(TransactionFragment transactionFragment, List<TransactionPojo> transactionPojos) {
                        transactionFragment.onTransactionsRequestSuccess(transactionPojos);
                    }
                },
                new Action2<TransactionFragment, Throwable>() {
                    @Override
                    public void call(TransactionFragment transactionFragment, Throwable throwable) {
                        mErrorHandler.handleError(transactionFragment.getActivity(), throwable);
                    }
                }
        );
    }

    public void requestTransactions() {
        start(mRequestId);
    }
}
