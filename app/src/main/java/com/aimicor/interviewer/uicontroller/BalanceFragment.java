package com.aimicor.interviewer.uicontroller;

import com.aimicor.interviewer.R;
import com.aimicor.interviewer.presenter.BalancePresenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusFragment;

import static android.view.View.GONE;

@RequiresPresenter(value = BalancePresenter.class)
public class BalanceFragment extends NucleusFragment<BalancePresenter> {

    private TextView mBalance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_balance, container, false);
        mBalance = (TextView) root.findViewById(R.id.balance);
        if(savedInstanceState == null){
            getPresenter().requestBalance();
        }
        return root;
    }

    public void onBalanceRequestSuccess(String response) {
        mBalance.setText("£" + response);
        mBalance.getRootView().findViewById(R.id.progress_bar).setVisibility(GONE);
    }
}
