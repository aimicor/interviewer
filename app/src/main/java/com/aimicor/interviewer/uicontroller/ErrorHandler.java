package com.aimicor.interviewer.uicontroller;

import android.app.Activity;

public interface ErrorHandler {

    void handleError(Activity activity, Throwable throwable);
}
