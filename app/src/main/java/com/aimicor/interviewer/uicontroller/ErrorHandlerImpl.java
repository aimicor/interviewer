package com.aimicor.interviewer.uicontroller;

import com.aimicor.interviewer.R;

import android.app.Activity;
import android.widget.Toast;

import static android.view.View.GONE;

class ErrorHandlerImpl implements ErrorHandler {

    @Override
    public void handleError(Activity activity, Throwable throwable) {
        Toast.makeText(activity, throwable.getMessage(), Toast.LENGTH_LONG).show();
        activity.findViewById(R.id.progress_bar).setVisibility(GONE);
    }
}
