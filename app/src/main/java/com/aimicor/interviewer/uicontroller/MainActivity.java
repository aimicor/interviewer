package com.aimicor.interviewer.uicontroller;

import com.aimicor.interviewer.R;
import com.aimicor.interviewer.presenter.MainPresenter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusActivity;

import static android.view.View.VISIBLE;

@RequiresPresenter(value = MainPresenter.class)
public class MainActivity extends NucleusActivity<MainPresenter> {

    private View mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.progress_bar);
        if (savedInstanceState == null) {
            mProgressBar.setVisibility(VISIBLE);
            getPresenter().requestToken();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void onButtonClick(View button) {
        switch(button.getId()){
            case R.id.spend:
                show(new SpendFragment());
                break;
            case R.id.transactions:
                mProgressBar.setVisibility(VISIBLE);
                show(new TransactionFragment());
                break;
            default:
                mProgressBar.setVisibility(VISIBLE);
                show(new BalanceFragment());
        }
    }

    public void onTokenRequestSuccess() {
        onButtonClick(new View(getApplicationContext()));
    }

    private void show(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.main_content, fragment)
                .commit();
    }
}
