package com.aimicor.interviewer.uicontroller;

import com.aimicor.interviewer.R;
import com.aimicor.interviewer.model.TransactionPojo;
import com.aimicor.interviewer.presenter.SpendPresenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusFragment;

import static android.view.View.VISIBLE;

@RequiresPresenter(value = SpendPresenter.class)
public class SpendFragment extends NucleusFragment<SpendPresenter> implements View.OnClickListener {

    private TextView mDescription;
    private TextView mAmount;
    private View mProgressBar;
    private View mSuccessButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_spend, container, false);
        mDescription = (TextView) view.findViewById(R.id.description);
        mAmount = (TextView) view.findViewById(R.id.amount);
        mProgressBar = container.getRootView().findViewById(R.id.progress_bar);
        mSuccessButton = container.getRootView().findViewById(R.id.button);
        view.findViewById(R.id.spend).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        mProgressBar.setVisibility(VISIBLE);
        getPresenter().sendTransaction(new TransactionPojo.Builder()
                .amount(mAmount.getText())
                .description(mDescription.getText())
                .build());
    }

    public void onTransactionSendSuccess() {
        mSuccessButton.callOnClick();
    }
}
