package com.aimicor.interviewer.uicontroller;

import com.aimicor.interviewer.R;
import com.aimicor.interviewer.model.TransactionPojo;
import com.aimicor.interviewer.presenter.TransactionPresenter;
import com.aimicor.interviewer.uicontroller.adapter.RecyclerViewAdapter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusFragment;

import static android.view.View.GONE;
import static com.aimicor.interviewer.uicontroller.adapter.AdapterModule.transactionItemAdapterProvider;

@RequiresPresenter(value = TransactionPresenter.class)
public class TransactionFragment extends NucleusFragment<TransactionPresenter> {

    private final RecyclerViewAdapter<TransactionPojo> mAdapter;
    private RecyclerView mRecyclerView;

    public TransactionFragment() {
        this(transactionItemAdapterProvider());
    }

    @SuppressLint("ValidFragment")
    private TransactionFragment(RecyclerViewAdapter<TransactionPojo> adapter) {
        mAdapter = adapter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_transactions, container, false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.setAdapter(mAdapter.getAdapter());

        if(savedInstanceState == null) {
            getPresenter().requestTransactions();
        }

        return mRecyclerView;
    }

    public void onTransactionsRequestSuccess(List<TransactionPojo> transactionPojos) {
        mRecyclerView.getRootView().findViewById(R.id.progress_bar).setVisibility(GONE);
        mAdapter.setItems(transactionPojos);
    }
}
