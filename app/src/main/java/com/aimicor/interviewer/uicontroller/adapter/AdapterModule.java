package com.aimicor.interviewer.uicontroller.adapter;

import com.aimicor.interviewer.model.TransactionPojo;

public class AdapterModule {

    public static RecyclerViewAdapter<TransactionPojo> transactionItemAdapterProvider() {
        return new TransactionItemAdapter();
    }
}
