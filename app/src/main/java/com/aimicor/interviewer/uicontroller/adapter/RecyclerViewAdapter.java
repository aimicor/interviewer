package com.aimicor.interviewer.uicontroller.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

public interface RecyclerViewAdapter<T> {

    void setItems(List<T> items);

    RecyclerView.Adapter getAdapter();
}
