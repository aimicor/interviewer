package com.aimicor.interviewer.uicontroller.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

abstract class RecyclerViewAdapterBase<HOLDER extends RecyclerView.ViewHolder, ITEM>
        extends RecyclerView.Adapter<HOLDER>
        implements RecyclerViewAdapter<ITEM> {

    private final List<ITEM> mItems = new ArrayList<>();

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void setItems(List<ITEM> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        return this;
    }

    ITEM getItem(int index){
        return mItems.get(index);
    }
}
