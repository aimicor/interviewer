package com.aimicor.interviewer.uicontroller.adapter;

import com.aimicor.interviewer.R;
import com.aimicor.interviewer.model.TransactionPojo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.aimicor.interviewer.model.TransactionPojo.POJO_DATE_FORMAT;
import static java.util.Locale.UK;

class TransactionItemAdapter extends RecyclerViewAdapterBase<TransactionItemViewHolder, TransactionPojo> {

    private static final String NEW_DATE_FORMAT = "dd/MM/yyyy";

    @Override
    public TransactionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transaction, parent, false);
        return new TransactionItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TransactionItemViewHolder holder, int position) {
        holder.getDate().setText(format(getItem(position).getDate()));
        holder.getDescription().setText(getItem(position).getDescription());
        holder.getAmount().setText("£" + getItem(position).getAmount());
    }

    private String format(String dateString) {
        try {
            Date date = new SimpleDateFormat(POJO_DATE_FORMAT, UK).parse(dateString);
            return new SimpleDateFormat(NEW_DATE_FORMAT, UK).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
