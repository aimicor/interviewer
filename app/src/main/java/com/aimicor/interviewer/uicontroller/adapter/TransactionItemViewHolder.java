package com.aimicor.interviewer.uicontroller.adapter;

import com.aimicor.interviewer.R;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

class TransactionItemViewHolder extends RecyclerView.ViewHolder {

    private final TextView mDate;
    private final TextView mDescription;
    private final TextView mAmount;

    TransactionItemViewHolder(View itemView) {
        super(itemView);
        mDate = (TextView) itemView.findViewById(R.id.date);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mAmount = (TextView) itemView.findViewById(R.id.amount);
    }

    TextView getDate() {
        return mDate;
    }

    TextView getDescription() {
        return mDescription;
    }

    TextView getAmount() {
        return mAmount;
    }
}
