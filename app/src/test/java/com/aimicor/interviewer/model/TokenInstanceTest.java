package com.aimicor.interviewer.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import com.aimicor.interviewer.model.TokenInstance.Token;
import com.aimicor.interviewer.model.TokenInstance.TokenPojo;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TokenInstanceTest {

    @Test
    public void can_set_member_like_pojo_with_json() {
        final String tokenString = "pbzguwtjkjothmnzmi";
        String rawJson = "{\n\"token\":\"" + tokenString + "\"\n}";
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(rawJson);
        Token token = new Token();
        token.set(new Gson().fromJson(json, TokenPojo.class));
        assertThat(token.get()).isEqualTo("Bearer " + tokenString);
    }
}