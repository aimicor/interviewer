package com.aimicor.interviewer.model;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.aimicor.interviewer.model.TransactionPojo.POJO_DATE_FORMAT;
import static java.util.Locale.UK;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionPojoTest {

    @Test
    public void correct_timestamp_format_generated() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(POJO_DATE_FORMAT, UK);
        String formattedDate = sdf.format(date);

        assertThat(new TransactionPojo.Builder().build().getDate()).isEqualTo(formattedDate);
    }

}