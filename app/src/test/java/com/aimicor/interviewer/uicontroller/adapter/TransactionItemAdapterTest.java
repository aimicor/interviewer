package com.aimicor.interviewer.uicontroller.adapter;

import com.aimicor.interviewer.model.TransactionPojo;

import org.junit.Test;

import android.widget.TextView;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransactionItemAdapterTest {

    @Test
    public void dates_formatted_properly() {
        TransactionItemAdapter subject = new TransactionItemAdapter();
        TransactionItemViewHolder holder = mock(TransactionItemViewHolder.class);
        TextView date = mock(TextView.class);
        TransactionPojo pojo = mock(TransactionPojo.class);
        when(holder.getAmount()).thenReturn(mock(TextView.class));
        when(holder.getDescription()).thenReturn(mock(TextView.class));
        when(holder.getDate()).thenReturn(date);
        when(pojo.getDate()).thenReturn("2016-12-11T12:23:34Z");
        final ArrayList<TransactionPojo> items = new ArrayList<>();
        items.add(pojo);
        subject.setItems(items);

        subject.onBindViewHolder(holder, 0);

        verify(date).setText("11/12/2016");
    }
}